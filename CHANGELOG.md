# [2.1.0](https://gitlab.com/to-be-continuous/terraform/compare/2.0.0...2.1.0) (2021-09-19)


### Features

* add infracost ([1ffba7f](https://gitlab.com/to-be-continuous/terraform/commit/1ffba7f5eb3679c8c314d6e9fb8a696f31525a09))

## [2.0.0](https://gitlab.com/to-be-continuous/terraform/compare/1.1.2...2.0.0) (2021-09-08)

### Features

* Change boolean variable behaviour ([f175aed](https://gitlab.com/to-be-continuous/terraform/commit/f175aed5f75a03802e70e7736c429c132e8ca565))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.1.2](https://gitlab.com/to-be-continuous/terraform/compare/1.1.1...1.1.2) (2021-07-21)

### Bug Fixes

* remove dependencies on tfsec, tflint and checkcov ([f8f1e87](https://gitlab.com/to-be-continuous/terraform/commit/f8f1e871574c8a14ae36002efe2d9a0288476ad0))

## [1.1.1](https://gitlab.com/to-be-continuous/terraform/compare/1.1.0...1.1.1) (2021-07-08)

### Bug Fixes

* conflict between vault and scoped vars ([27ebea7](https://gitlab.com/to-be-continuous/terraform/commit/27ebea71b1f872200ccf3f0d1d7c08f1fb9d8e3d))

## [1.1.0](https://gitlab.com/to-be-continuous/terraform/compare/1.0.0...1.1.0) (2021-06-10)

### Features

* move group ([869658c](https://gitlab.com/to-be-continuous/terraform/commit/869658cd53feb93b984028556114c318f872fc9b))

## 1.0.0 (2021-05-06)

### Features

* initial release ([9421d94](https://gitlab.com/Orange-OpenSource/tbc/terraform/commit/9421d94e6fd8149da03831dcda3723baaed7d745))
